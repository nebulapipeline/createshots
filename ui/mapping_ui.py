# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mayaapps\createshots\ui\mapping.ui'
#
# Created: Mon Nov 22 19:15:19 2021
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from Qt import QtCompat, QtCore, QtGui, QtWidgets

class Ui_Frame(object):
    def setupUi(self, Frame):
        Frame.setObjectName("Frame")
        Frame.resize(315, 41)
        Frame.setFrameShape(QtWidgets.QFrame.Panel)
        Frame.setFrameShadow(QtWidgets.QFrame.Plain)
        self.horizontalLayout = QtWidgets.QHBoxLayout(Frame)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.cacheLabel = QtWidgets.QLabel(Frame)
        self.cacheLabel.setObjectName("cacheLabel")
        self.horizontalLayout.addWidget(self.cacheLabel)
        self.fileLabel = QtWidgets.QLabel(Frame)
        self.fileLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.fileLabel.setObjectName("fileLabel")
        self.horizontalLayout.addWidget(self.fileLabel)
        self.removeLabel = QtWidgets.QLabel(Frame)
        self.removeLabel.setMinimumSize(QtCore.QSize(10, 10))
        self.removeLabel.setMaximumSize(QtCore.QSize(10, 10))
        self.removeLabel.setObjectName("removeLabel")
        self.horizontalLayout.addWidget(self.removeLabel)
        self.ldBox = QtWidgets.QComboBox(Frame)
        self.ldBox.setObjectName("ldBox")
        self.ldBox.addItem("")
        self.ldBox.setItemText(0, "")
        self.horizontalLayout.addWidget(self.ldBox)
        self.browseButton = QtWidgets.QToolButton(Frame)
        self.browseButton.setObjectName("browseButton")
        self.horizontalLayout.addWidget(self.browseButton)

        self.retranslateUi(Frame)
        QtCore.QMetaObject.connectSlotsByName(Frame)

    def retranslateUi(self, Frame):
        Frame.setWindowTitle(QtCompat.translate("Frame", "Frame", None, -1))
        self.cacheLabel.setText(QtCompat.translate("Frame", "TextLabel", None, -1))
        self.fileLabel.setText(QtCompat.translate("Frame", "TextLabel", None, -1))
        self.removeLabel.setToolTip(QtCompat.translate("Frame", "Remove file", None, -1))
        self.removeLabel.setText(QtCompat.translate("Frame", "TextLabel", None, -1))
        self.ldBox.setToolTip(QtCompat.translate("Frame", "Select a mesh to apply corresponding cache file", None, -1))
        self.browseButton.setToolTip(QtCompat.translate("Frame", "Add a reference to the mesh file for this cache", None, -1))
        self.browseButton.setText(QtCompat.translate("Frame", "...", None, -1))

