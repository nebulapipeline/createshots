# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mayaapps\createshots\ui\item.ui'
#
# Created: Mon Nov 22 19:15:19 2021
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(638, 139)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.titleFrame = QtWidgets.QFrame(Form)
        self.titleFrame.setMinimumSize(QtCore.QSize(0, 25))
        self.titleFrame.setCursor(QtCore.Qt.PointingHandCursor)
        self.titleFrame.setFrameShape(QtWidgets.QFrame.Panel)
        self.titleFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.titleFrame.setLineWidth(2)
        self.titleFrame.setObjectName("titleFrame")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.titleFrame)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setContentsMargins(4, 0, 2, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.nameLabel = QtWidgets.QLabel(self.titleFrame)
        self.nameLabel.setMinimumSize(QtCore.QSize(0, 0))
        self.nameLabel.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.nameLabel.setFont(font)
        self.nameLabel.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.nameLabel.setFrameShadow(QtWidgets.QFrame.Plain)
        self.nameLabel.setText("")
        self.nameLabel.setObjectName("nameLabel")
        self.horizontalLayout.addWidget(self.nameLabel)
        self.iconLabel = QtWidgets.QLabel(self.titleFrame)
        self.iconLabel.setMinimumSize(QtCore.QSize(15, 0))
        self.iconLabel.setText("")
        self.iconLabel.setObjectName("iconLabel")
        self.horizontalLayout.addWidget(self.iconLabel)
        self.verticalLayout_2.addWidget(self.titleFrame)
        self.frame = QtWidgets.QFrame(Form)
        self.frame.setStyleSheet("")
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setObjectName("frame")
        self.itemLayout = QtWidgets.QVBoxLayout(self.frame)
        self.itemLayout.setObjectName("itemLayout")
        self.renderLayerLayout = QtWidgets.QHBoxLayout()
        self.renderLayerLayout.setObjectName("renderLayerLayout")
        self.label_2 = QtWidgets.QLabel(self.frame)
        self.label_2.setObjectName("label_2")
        self.renderLayerLayout.addWidget(self.label_2)
        self.singleFrameButton = QtWidgets.QCheckBox(self.frame)
        self.singleFrameButton.setChecked(False)
        self.singleFrameButton.setObjectName("singleFrameButton")
        self.renderLayerLayout.addWidget(self.singleFrameButton)
        self.line = QtWidgets.QFrame(self.frame)
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.renderLayerLayout.addWidget(self.line)
        self.overrideButton = QtWidgets.QCheckBox(self.frame)
        self.overrideButton.setObjectName("overrideButton")
        self.renderLayerLayout.addWidget(self.overrideButton)
        self.label_3 = QtWidgets.QLabel(self.frame)
        self.label_3.setObjectName("label_3")
        self.renderLayerLayout.addWidget(self.label_3)
        self.startFrameBox = QtWidgets.QSpinBox(self.frame)
        self.startFrameBox.setMinimumSize(QtCore.QSize(100, 0))
        self.startFrameBox.setMaximum(999999999)
        self.startFrameBox.setObjectName("startFrameBox")
        self.renderLayerLayout.addWidget(self.startFrameBox)
        self.label_4 = QtWidgets.QLabel(self.frame)
        self.label_4.setObjectName("label_4")
        self.renderLayerLayout.addWidget(self.label_4)
        self.endFrameBox = QtWidgets.QSpinBox(self.frame)
        self.endFrameBox.setMinimumSize(QtCore.QSize(100, 0))
        self.endFrameBox.setMaximum(999999999)
        self.endFrameBox.setObjectName("endFrameBox")
        self.renderLayerLayout.addWidget(self.endFrameBox)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.renderLayerLayout.addItem(spacerItem)
        self.line_2 = QtWidgets.QFrame(self.frame)
        self.line_2.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.renderLayerLayout.addWidget(self.line_2)
        self.label = QtWidgets.QLabel(self.frame)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.renderLayerLayout.addWidget(self.label)
        self.itemLayout.addLayout(self.renderLayerLayout)
        self.verticalLayout_2.addWidget(self.frame)

        self.retranslateUi(Form)
        QtCore.QObject.connect(self.singleFrameButton, QtCore.SIGNAL("toggled(bool)"), self.startFrameBox.setDisabled)
        QtCore.QObject.connect(self.singleFrameButton, QtCore.SIGNAL("toggled(bool)"), self.endFrameBox.setDisabled)
        QtCore.QObject.connect(self.singleFrameButton, QtCore.SIGNAL("toggled(bool)"), self.overrideButton.setDisabled)
        QtCore.QObject.connect(self.overrideButton, QtCore.SIGNAL("toggled(bool)"), self.label_3.setVisible)
        QtCore.QObject.connect(self.overrideButton, QtCore.SIGNAL("toggled(bool)"), self.startFrameBox.setVisible)
        QtCore.QObject.connect(self.overrideButton, QtCore.SIGNAL("toggled(bool)"), self.label_4.setVisible)
        QtCore.QObject.connect(self.overrideButton, QtCore.SIGNAL("toggled(bool)"), self.endFrameBox.setVisible)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtWidgets.QApplication.translate("Form", "Form", None, -1))
        self.label_2.setText(QtWidgets.QApplication.translate("Form", "Env Layer: ", None, -1))
        self.singleFrameButton.setToolTip(QtWidgets.QApplication.translate("Form", "Configure single frame for rendering for the Env layer", None, -1))
        self.singleFrameButton.setText(QtWidgets.QApplication.translate("Form", "Single Frame", None, -1))
        self.overrideButton.setToolTip(QtWidgets.QApplication.translate("Form", "Override the frame range for Env layer", None, -1))
        self.overrideButton.setText(QtWidgets.QApplication.translate("Form", "Override", None, -1))
        self.label_3.setText(QtWidgets.QApplication.translate("Form", "Start", None, -1))
        self.startFrameBox.setToolTip(QtWidgets.QApplication.translate("Form", "Start frame to override", None, -1))
        self.label_4.setText(QtWidgets.QApplication.translate("Form", "End", None, -1))
        self.endFrameBox.setToolTip(QtWidgets.QApplication.translate("Form", "End frame to override", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("Form", "Render Layers: ", None, -1))

