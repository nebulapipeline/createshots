# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mayaapps\createshots\ui\main.ui'
#
# Created: Mon Nov 22 19:15:19 2021
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(767, 585)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.shotsLayout = QtWidgets.QHBoxLayout()
        self.shotsLayout.setObjectName("shotsLayout")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.shotsLayout.addWidget(self.label_2)
        self.shotsFilePathBox = QtWidgets.QLineEdit(self.centralwidget)
        self.shotsFilePathBox.setObjectName("shotsFilePathBox")
        self.shotsLayout.addWidget(self.shotsFilePathBox)
        self.browseButton2 = QtWidgets.QToolButton(self.centralwidget)
        self.browseButton2.setObjectName("browseButton2")
        self.shotsLayout.addWidget(self.browseButton2)
        self.verticalLayout_2.addLayout(self.shotsLayout)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.csvFilePathBox = QtWidgets.QLineEdit(self.centralwidget)
        self.csvFilePathBox.setObjectName("csvFilePathBox")
        self.horizontalLayout.addWidget(self.csvFilePathBox)
        self.browseButton = QtWidgets.QToolButton(self.centralwidget)
        self.browseButton.setObjectName("browseButton")
        self.horizontalLayout.addWidget(self.browseButton)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.statusLabel = QtWidgets.QLabel(self.centralwidget)
        self.statusLabel.setText("")
        self.statusLabel.setObjectName("statusLabel")
        self.verticalLayout_2.addWidget(self.statusLabel)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.statusBox = QtWidgets.QTextEdit(self.centralwidget)
        self.statusBox.setLineWrapMode(QtWidgets.QTextEdit.NoWrap)
        self.statusBox.setReadOnly(True)
        self.statusBox.setObjectName("statusBox")
        self.verticalLayout.addWidget(self.statusBox)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_4.addWidget(self.label_3)
        self.outputPathBox = QtWidgets.QLineEdit(self.centralwidget)
        self.outputPathBox.setObjectName("outputPathBox")
        self.horizontalLayout_4.addWidget(self.outputPathBox)
        self.browseButton1 = QtWidgets.QToolButton(self.centralwidget)
        self.browseButton1.setObjectName("browseButton1")
        self.horizontalLayout_4.addWidget(self.browseButton1)
        self.verticalLayout_2.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.saveToLocalButton = QtWidgets.QCheckBox(self.centralwidget)
        self.saveToLocalButton.setObjectName("saveToLocalButton")
        self.horizontalLayout_3.addWidget(self.saveToLocalButton)
        self.resolutionBox = QtWidgets.QComboBox(self.centralwidget)
        self.resolutionBox.setObjectName("resolutionBox")
        self.horizontalLayout_3.addWidget(self.resolutionBox)
        self.useRendersButton = QtWidgets.QCheckBox(self.centralwidget)
        self.useRendersButton.setObjectName("useRendersButton")
        self.horizontalLayout_3.addWidget(self.useRendersButton)
        self.createFilesButton = QtWidgets.QCheckBox(self.centralwidget)
        self.createFilesButton.setObjectName("createFilesButton")
        self.horizontalLayout_3.addWidget(self.createFilesButton)
        self.createCollageButton = QtWidgets.QCheckBox(self.centralwidget)
        self.createCollageButton.setObjectName("createCollageButton")
        self.horizontalLayout_3.addWidget(self.createCollageButton)
        self.startButton = QtWidgets.QPushButton(self.centralwidget)
        self.startButton.setMinimumSize(QtCore.QSize(75, 0))
        self.startButton.setObjectName("startButton")
        self.horizontalLayout_3.addWidget(self.startButton)
        self.stopButton = QtWidgets.QPushButton(self.centralwidget)
        self.stopButton.setMinimumSize(QtCore.QSize(75, 0))
        self.stopButton.setObjectName("stopButton")
        self.horizontalLayout_3.addWidget(self.stopButton)
        self.closeButton = QtWidgets.QPushButton(self.centralwidget)
        self.closeButton.setMinimumSize(QtCore.QSize(75, 0))
        self.closeButton.setObjectName("closeButton")
        self.horizontalLayout_3.addWidget(self.closeButton)
        self.verticalLayout_2.addLayout(self.horizontalLayout_3)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 767, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)

        self.retranslateUi(MainWindow)
        QtCore.QObject.connect(self.closeButton, QtCore.SIGNAL("clicked()"), MainWindow.close)
        QtCore.QObject.connect(self.createFilesButton, QtCore.SIGNAL("toggled(bool)"), self.saveToLocalButton.setVisible)
        QtCore.QObject.connect(self.saveToLocalButton, QtCore.SIGNAL("toggled(bool)"), self.label_3.setVisible)
        QtCore.QObject.connect(self.saveToLocalButton, QtCore.SIGNAL("toggled(bool)"), self.outputPathBox.setVisible)
        QtCore.QObject.connect(self.saveToLocalButton, QtCore.SIGNAL("toggled(bool)"), self.browseButton1.setVisible)
        QtCore.QObject.connect(self.createCollageButton, QtCore.SIGNAL("toggled(bool)"), self.useRendersButton.setVisible)
        QtCore.QObject.connect(self.useRendersButton, QtCore.SIGNAL("toggled(bool)"), self.resolutionBox.setVisible)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtWidgets.QApplication.translate("MainWindow", "Create Shots", None, -1))
        self.label_2.setText(QtWidgets.QApplication.translate("MainWindow", "Shots Dir   ", None, -1))
        self.shotsFilePathBox.setToolTip(QtWidgets.QApplication.translate("MainWindow", "Add directory where you see list of shots", None, -1))
        self.browseButton2.setToolTip(QtWidgets.QApplication.translate("MainWindow", "Browse for directory", None, -1))
        self.browseButton2.setText(QtWidgets.QApplication.translate("MainWindow", "...", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("MainWindow", "CSV File    ", None, -1))
        self.csvFilePathBox.setToolTip(QtWidgets.QApplication.translate("MainWindow", "Add path to CSV file", None, -1))
        self.browseButton.setText(QtWidgets.QApplication.translate("MainWindow", "...", None, -1))
        self.label_3.setText(QtWidgets.QApplication.translate("MainWindow", "Output Dir", None, -1))
        self.outputPathBox.setToolTip(QtWidgets.QApplication.translate("MainWindow", "Path to save Maya files", None, -1))
        self.browseButton1.setText(QtWidgets.QApplication.translate("MainWindow", "...", None, -1))
        self.saveToLocalButton.setToolTip(QtWidgets.QApplication.translate("MainWindow", "Specify a location to save Maya files", None, -1))
        self.saveToLocalButton.setText(QtWidgets.QApplication.translate("MainWindow", "Add Location", None, -1))
        self.resolutionBox.setToolTip(QtWidgets.QApplication.translate("MainWindow", "Select Resolution", None, -1))
        self.useRendersButton.setToolTip(QtWidgets.QApplication.translate("MainWindow", "Create Collage using renders", None, -1))
        self.useRendersButton.setText(QtWidgets.QApplication.translate("MainWindow", "Use Renders", None, -1))
        self.createFilesButton.setToolTip(QtWidgets.QApplication.translate("MainWindow", "Create and save Maya files", None, -1))
        self.createFilesButton.setText(QtWidgets.QApplication.translate("MainWindow", "Create Files", None, -1))
        self.createCollageButton.setToolTip(QtWidgets.QApplication.translate("MainWindow", "Take snapshots for each Shot and create a Collage", None, -1))
        self.createCollageButton.setText(QtWidgets.QApplication.translate("MainWindow", "Create Collage", None, -1))
        self.startButton.setText(QtWidgets.QApplication.translate("MainWindow", "Start", None, -1))
        self.stopButton.setText(QtWidgets.QApplication.translate("MainWindow", "Stop", None, -1))
        self.closeButton.setText(QtWidgets.QApplication.translate("MainWindow", "Close", None, -1))

